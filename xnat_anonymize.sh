#!/bin/nash

# This snippet will anonymize all DICOMS in a given project on XNAT, but must
# be run with root priveleges on the XNAT VM, in the project's main archive.

# IT WILL NEED TO BE MODIFIED TO WORK FOR YOUR PROJECT

# $root_path MUST BE THE PATH TO THE ARCHIVE FOR THE PROJECT

root_path=/data/archive/vasc_disease/arc001
cd $root_path
for k in *; do

    # $subject NEEDS TO BE THE DESIRED SUBJECT NAME, in this case, for example, 
    # $k=elijahMRI03132017 and this echo will isolate the subject name from the 
    # session names, assuming a session name in the above format.
    
    subject=$(echo $k | cut -dM -f1 | cut -d0 -f1)
    cd $root_path/$k/SCANS
    home_path=$PWD

    echo "HOME: $home_path"
    for i in *; do
        echo "SERIES: $i"
        cd ${i}/DICOM
        pwd
        for j in `ls`; do
            
            # ADD TAGS TO BE ANONYMIZED HERE AS NEEDED
            dcmodify -nb -q -ma "(0010,0010)=$subject" $j
            dcmodify -nb -q -ma "(0010,0020)=${k}" $j
            dcmodify -nb -q -ma "(0010,0030)=DOB" $j
            dcmodify -nb -q -ma "(0010,1040)=address" $j
            dcmodify -nb -q -ma "(0008,2111)=derivation_description" $j
            dcmodify -nb -q -ma "(0010,1000)=other_patient_id" $j
            dcmodify -nb -q -ma "(0010,1001)=other_patient_names" $j
            dcmodify -nb -q -ma "(0010,1005)=other_birth_name" $j
        done
        cd $home_path
        cd ${i}/secondary
        pwd
        for j in `ls`; do
        
            # ADD TAGS TO BE ANONYMIZED HERE AS NEEDED
            dcmodify -nb -q -ma "(0010,0010)=$subject" $j
            dcmodify -nb -q -ma "(0010,0020)=${k}" $j
            dcmodify -nb -q -ma "(0010,0030)=DOB" $j
            dcmodify -nb -q -ma "(0010,1040)=address" $j
            dcmodify -nb -q -ma "(0008,2111)=derivation_description" $j
            dcmodify -nb -q -ma "(0010,1000)=other_patient_id" $j
            dcmodify -nb -q -ma "(0010,1001)=other_patient_names" $j
            dcmodify -nb -q -ma "(0010,1005)=other_birth_name" $j
        done
        cd $home_path
    done
done